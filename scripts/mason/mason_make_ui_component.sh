#!/bin/sh

DIR_NAME="order_product"
CLASS_NAME="OrderProductItem"
FILE_PATH="/src/components/src/items/src/"
BRICK_NAME="ui_component"

cd ../../ &&
ls &&
mason make $BRICK_NAME -o ./lib$FILE_PATH --dirname $DIR_NAME --classname $CLASS_NAME

cd scripts/auto_build &&
sh autobuild.sh
