part of '../../index.dart';

class ProgressInfoColors extends ThemeExtension<ProgressInfoColors> {
  ProgressInfoColors({
    required this.fill,
    required this.text,
    required this.icon,
  });

  final Color fill;
  final Color text;
  final Color icon;

  @override
  ProgressInfoColors lerp(ProgressInfoColors? other, double t) {
    if (other is! ProgressInfoColors) {
      return this;
    }

    return ProgressInfoColors(
      fill: Color.lerp(fill, other.fill, t)!,
      text: Color.lerp(text, other.text, t)!,
      icon: Color.lerp(icon, other.icon, t)!,
    );
  }

  @override
  ProgressInfoColors copyWith({
    Color? fill,
    Color? text,
    Color? icon,
  }) {
    return ProgressInfoColors(
      fill: fill ?? this.fill,
      text: text ?? this.text,
      icon: icon ?? this.icon,
    );
  }
}
