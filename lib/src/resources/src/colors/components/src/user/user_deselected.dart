part of '../../index.dart';

class UserDeselectedColors extends ThemeExtension<UserDeselectedColors> {
  UserDeselectedColors({
    required this.defaultFill,
    required this.hoverFill,
    required this.pressedFill,
    required this.disabledFill,
    required this.focusedFill,
    required this.focusedBorder2,
    required this.defaultText,
    required this.hoverText,
    required this.pressedText,
    required this.disabledText,
    required this.focusedText,
  });

  final Color defaultFill;
  final Color hoverFill;
  final Color pressedFill;
  final Color disabledFill;
  final Color focusedFill;
  final Color focusedBorder2;
  final Color defaultText;
  final Color hoverText;
  final Color pressedText;
  final Color disabledText;
  final Color focusedText;

  @override
  UserDeselectedColors lerp(UserDeselectedColors? other, double t) {
    if (other is! UserDeselectedColors) {
      return this;
    }

    return UserDeselectedColors(
      defaultFill: Color.lerp(defaultFill, other.defaultFill, t)!,
      hoverFill: Color.lerp(hoverFill, other.hoverFill, t)!,
      pressedFill: Color.lerp(pressedFill, other.pressedFill, t)!,
      disabledFill: Color.lerp(disabledFill, other.disabledFill, t)!,
      focusedFill: Color.lerp(focusedFill, other.focusedFill, t)!,
      focusedBorder2: Color.lerp(focusedBorder2, other.focusedBorder2, t)!,
      defaultText: Color.lerp(defaultText, other.defaultText, t)!,
      hoverText: Color.lerp(hoverText, other.hoverText, t)!,
      pressedText: Color.lerp(pressedText, other.pressedText, t)!,
      disabledText: Color.lerp(disabledText, other.disabledText, t)!,
      focusedText: Color.lerp(focusedText, other.focusedText, t)!,
    );
  }

  @override
  UserDeselectedColors copyWith({
    Color? defaultFill,
    Color? hoverFill,
    Color? pressedFill,
    Color? disabledFill,
    Color? focusedFill,
    Color? focusedBorder2,
    Color? defaultText,
    Color? hoverText,
    Color? pressedText,
    Color? disabledText,
    Color? focusedText,
  }) {
    return UserDeselectedColors(
      defaultFill: defaultFill ?? this.defaultFill,
      hoverFill: hoverFill ?? this.hoverFill,
      pressedFill: pressedFill ?? this.pressedFill,
      disabledFill: disabledFill ?? this.disabledFill,
      focusedFill: focusedFill ?? this.focusedFill,
      focusedBorder2: focusedBorder2 ?? this.focusedBorder2,
      defaultText: defaultText ?? this.defaultText,
      hoverText: hoverText ?? this.hoverText,
      pressedText: pressedText ?? this.pressedText,
      disabledText: disabledText ?? this.disabledText,
      focusedText: focusedText ?? this.focusedText,
    );
  }
}
