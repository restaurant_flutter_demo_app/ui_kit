part of '../../index.dart';

class PictographicColors extends ThemeExtension<PictographicColors> {
  PictographicColors({
    required this.successFillBottom,
    required this.warningFillBottom,
    required this.errorFillBottom,
    required this.infoFillBottom,
    required this.neutralFillBottom,
    required this.purpleFillBottom,
    required this.orangeFillBottom,
    required this.greenFillBottom,
    required this.blueFillBottom,
    required this.redFillBottom,
    required this.grayFillBottom,
    required this.brandFillBottom,
    required this.successFillMiddle,
    required this.warningFillMiddle,
    required this.errorFillMiddle,
    required this.infoFillMiddle,
    required this.neutralFillMiddle,
    required this.purpleFillMiddle,
    required this.orangeFillMiddle,
    required this.greenFillMiddle,
    required this.blueFillMiddle,
    required this.redFillMiddle,
    required this.grayFillMiddle,
    required this.brandFillMiddle,
    required this.successFillTop,
    required this.warningFillTop,
    required this.errorFillTop,
    required this.infoFillTop,
    required this.neutralFillTop,
    required this.purpleFillTop,
    required this.orangeFillTop,
    required this.greenFillTop,
    required this.blueFillTop,
    required this.redFillTop,
    required this.grayFillTop,
    required this.brandFillTop,
    required this.successText,
    required this.warningText,
    required this.errorText,
    required this.infoText,
    required this.neutralText,
    required this.purpleText,
    required this.orangeText,
    required this.greenText,
    required this.blueText,
    required this.redText,
    required this.grayText,
    required this.brandText,
    required this.successIcon,
    required this.warningIcon,
    required this.errorIcon,
    required this.infoIcon,
    required this.neutralIcon,
    required this.purpleIcon,
    required this.orangeIcon,
    required this.greenIcon,
    required this.blueIcon,
    required this.redIcon,
    required this.grayIcon,
    required this.brandIcon,
  });

  final Color successFillBottom;
  final Color warningFillBottom;
  final Color errorFillBottom;
  final Color infoFillBottom;
  final Color neutralFillBottom;
  final Color purpleFillBottom;
  final Color orangeFillBottom;
  final Color greenFillBottom;
  final Color blueFillBottom;
  final Color redFillBottom;
  final Color grayFillBottom;
  final Color brandFillBottom;
  final Color successFillMiddle;
  final Color warningFillMiddle;
  final Color errorFillMiddle;
  final Color infoFillMiddle;
  final Color neutralFillMiddle;
  final Color purpleFillMiddle;
  final Color orangeFillMiddle;
  final Color greenFillMiddle;
  final Color blueFillMiddle;
  final Color redFillMiddle;
  final Color grayFillMiddle;
  final Color brandFillMiddle;
  final Color successFillTop;
  final Color warningFillTop;
  final Color errorFillTop;
  final Color infoFillTop;
  final Color neutralFillTop;
  final Color purpleFillTop;
  final Color orangeFillTop;
  final Color greenFillTop;
  final Color blueFillTop;
  final Color redFillTop;
  final Color grayFillTop;
  final Color brandFillTop;
  final Color successText;
  final Color warningText;
  final Color errorText;
  final Color infoText;
  final Color neutralText;
  final Color purpleText;
  final Color orangeText;
  final Color greenText;
  final Color blueText;
  final Color redText;
  final Color grayText;
  final Color brandText;
  final Color successIcon;
  final Color warningIcon;
  final Color errorIcon;
  final Color infoIcon;
  final Color neutralIcon;
  final Color purpleIcon;
  final Color orangeIcon;
  final Color greenIcon;
  final Color blueIcon;
  final Color redIcon;
  final Color grayIcon;
  final Color brandIcon;

  @override
  PictographicColors lerp(PictographicColors? other, double t) {
    if (other is! PictographicColors) {
      return this;
    }

    return PictographicColors(
      successFillBottom:
          Color.lerp(successFillBottom, other.successFillBottom, t)!,
      warningFillBottom:
          Color.lerp(warningFillBottom, other.warningFillBottom, t)!,
      errorFillBottom: Color.lerp(errorFillBottom, other.errorFillBottom, t)!,
      infoFillBottom: Color.lerp(infoFillBottom, other.infoFillBottom, t)!,
      neutralFillBottom:
          Color.lerp(neutralFillBottom, other.neutralFillBottom, t)!,
      purpleFillBottom:
          Color.lerp(purpleFillBottom, other.purpleFillBottom, t)!,
      orangeFillBottom:
          Color.lerp(orangeFillBottom, other.orangeFillBottom, t)!,
      greenFillBottom: Color.lerp(greenFillBottom, other.greenFillBottom, t)!,
      blueFillBottom: Color.lerp(blueFillBottom, other.blueFillBottom, t)!,
      redFillBottom: Color.lerp(redFillBottom, other.redFillBottom, t)!,
      grayFillBottom: Color.lerp(grayFillBottom, other.grayFillBottom, t)!,
      brandFillBottom: Color.lerp(brandFillBottom, other.brandFillBottom, t)!,
      successFillMiddle:
          Color.lerp(successFillMiddle, other.successFillMiddle, t)!,
      warningFillMiddle:
          Color.lerp(warningFillMiddle, other.warningFillMiddle, t)!,
      errorFillMiddle: Color.lerp(errorFillMiddle, other.errorFillMiddle, t)!,
      infoFillMiddle: Color.lerp(infoFillMiddle, other.infoFillMiddle, t)!,
      neutralFillMiddle:
          Color.lerp(neutralFillMiddle, other.neutralFillMiddle, t)!,
      purpleFillMiddle:
          Color.lerp(purpleFillMiddle, other.purpleFillMiddle, t)!,
      orangeFillMiddle:
          Color.lerp(orangeFillMiddle, other.orangeFillMiddle, t)!,
      greenFillMiddle: Color.lerp(greenFillMiddle, other.greenFillMiddle, t)!,
      blueFillMiddle: Color.lerp(blueFillMiddle, other.blueFillMiddle, t)!,
      redFillMiddle: Color.lerp(redFillMiddle, other.redFillMiddle, t)!,
      grayFillMiddle: Color.lerp(grayFillMiddle, other.grayFillMiddle, t)!,
      brandFillMiddle: Color.lerp(brandFillMiddle, other.brandFillMiddle, t)!,
      successFillTop: Color.lerp(successFillTop, other.successFillTop, t)!,
      warningFillTop: Color.lerp(warningFillTop, other.warningFillTop, t)!,
      errorFillTop: Color.lerp(errorFillTop, other.errorFillTop, t)!,
      infoFillTop: Color.lerp(infoFillTop, other.infoFillTop, t)!,
      neutralFillTop: Color.lerp(neutralFillTop, other.neutralFillTop, t)!,
      purpleFillTop: Color.lerp(purpleFillTop, other.purpleFillTop, t)!,
      orangeFillTop: Color.lerp(orangeFillTop, other.orangeFillTop, t)!,
      greenFillTop: Color.lerp(greenFillTop, other.greenFillTop, t)!,
      blueFillTop: Color.lerp(blueFillTop, other.blueFillTop, t)!,
      redFillTop: Color.lerp(redFillTop, other.redFillTop, t)!,
      grayFillTop: Color.lerp(grayFillTop, other.grayFillTop, t)!,
      brandFillTop: Color.lerp(brandFillTop, other.brandFillTop, t)!,
      successText: Color.lerp(successText, other.successText, t)!,
      warningText: Color.lerp(warningText, other.warningText, t)!,
      errorText: Color.lerp(errorText, other.errorText, t)!,
      infoText: Color.lerp(infoText, other.infoText, t)!,
      neutralText: Color.lerp(neutralText, other.neutralText, t)!,
      purpleText: Color.lerp(purpleText, other.purpleText, t)!,
      orangeText: Color.lerp(orangeText, other.orangeText, t)!,
      greenText: Color.lerp(greenText, other.greenText, t)!,
      blueText: Color.lerp(blueText, other.blueText, t)!,
      redText: Color.lerp(redText, other.redText, t)!,
      grayText: Color.lerp(grayText, other.grayText, t)!,
      brandText: Color.lerp(brandText, other.brandText, t)!,
      successIcon: Color.lerp(successIcon, other.successIcon, t)!,
      warningIcon: Color.lerp(warningIcon, other.warningIcon, t)!,
      errorIcon: Color.lerp(errorIcon, other.errorIcon, t)!,
      infoIcon: Color.lerp(infoIcon, other.infoIcon, t)!,
      neutralIcon: Color.lerp(neutralIcon, other.neutralIcon, t)!,
      purpleIcon: Color.lerp(purpleIcon, other.purpleIcon, t)!,
      orangeIcon: Color.lerp(orangeIcon, other.orangeIcon, t)!,
      greenIcon: Color.lerp(greenIcon, other.greenIcon, t)!,
      blueIcon: Color.lerp(blueIcon, other.blueIcon, t)!,
      redIcon: Color.lerp(redIcon, other.redIcon, t)!,
      grayIcon: Color.lerp(grayIcon, other.grayIcon, t)!,
      brandIcon: Color.lerp(brandIcon, other.brandIcon, t)!,
    );
  }

  @override
  PictographicColors copyWith({
    Color? successFillBottom,
    Color? warningFillBottom,
    Color? errorFillBottom,
    Color? infoFillBottom,
    Color? neutralFillBottom,
    Color? purpleFillBottom,
    Color? orangeFillBottom,
    Color? greenFillBottom,
    Color? blueFillBottom,
    Color? redFillBottom,
    Color? grayFillBottom,
    Color? brandFillBottom,
    Color? successFillMiddle,
    Color? warningFillMiddle,
    Color? errorFillMiddle,
    Color? infoFillMiddle,
    Color? neutralFillMiddle,
    Color? purpleFillMiddle,
    Color? orangeFillMiddle,
    Color? greenFillMiddle,
    Color? blueFillMiddle,
    Color? redFillMiddle,
    Color? grayFillMiddle,
    Color? brandFillMiddle,
    Color? successFillTop,
    Color? warningFillTop,
    Color? errorFillTop,
    Color? infoFillTop,
    Color? neutralFillTop,
    Color? purpleFillTop,
    Color? orangeFillTop,
    Color? greenFillTop,
    Color? blueFillTop,
    Color? redFillTop,
    Color? grayFillTop,
    Color? brandFillTop,
    Color? successText,
    Color? warningText,
    Color? errorText,
    Color? infoText,
    Color? neutralText,
    Color? purpleText,
    Color? orangeText,
    Color? greenText,
    Color? blueText,
    Color? redText,
    Color? grayText,
    Color? brandText,
    Color? successIcon,
    Color? warningIcon,
    Color? errorIcon,
    Color? infoIcon,
    Color? neutralIcon,
    Color? purpleIcon,
    Color? orangeIcon,
    Color? greenIcon,
    Color? blueIcon,
    Color? redIcon,
    Color? grayIcon,
    Color? brandIcon,
  }) {
    return PictographicColors(
      successFillBottom: successFillBottom ?? this.successFillBottom,
      warningFillBottom: warningFillBottom ?? this.warningFillBottom,
      errorFillBottom: errorFillBottom ?? this.errorFillBottom,
      infoFillBottom: infoFillBottom ?? this.infoFillBottom,
      neutralFillBottom: neutralFillBottom ?? this.neutralFillBottom,
      purpleFillBottom: purpleFillBottom ?? this.purpleFillBottom,
      orangeFillBottom: orangeFillBottom ?? this.orangeFillBottom,
      greenFillBottom: greenFillBottom ?? this.greenFillBottom,
      blueFillBottom: blueFillBottom ?? this.blueFillBottom,
      redFillBottom: redFillBottom ?? this.redFillBottom,
      grayFillBottom: grayFillBottom ?? this.grayFillBottom,
      brandFillBottom: brandFillBottom ?? this.brandFillBottom,
      successFillMiddle: successFillMiddle ?? this.successFillMiddle,
      warningFillMiddle: warningFillMiddle ?? this.warningFillMiddle,
      errorFillMiddle: errorFillMiddle ?? this.errorFillMiddle,
      infoFillMiddle: infoFillMiddle ?? this.infoFillMiddle,
      neutralFillMiddle: neutralFillMiddle ?? this.neutralFillMiddle,
      purpleFillMiddle: purpleFillMiddle ?? this.purpleFillMiddle,
      orangeFillMiddle: orangeFillMiddle ?? this.orangeFillMiddle,
      greenFillMiddle: greenFillMiddle ?? this.greenFillMiddle,
      blueFillMiddle: blueFillMiddle ?? this.blueFillMiddle,
      redFillMiddle: redFillMiddle ?? this.redFillMiddle,
      grayFillMiddle: grayFillMiddle ?? this.grayFillMiddle,
      brandFillMiddle: brandFillMiddle ?? this.brandFillMiddle,
      successFillTop: successFillTop ?? this.successFillTop,
      warningFillTop: warningFillTop ?? this.warningFillTop,
      errorFillTop: errorFillTop ?? this.errorFillTop,
      infoFillTop: infoFillTop ?? this.infoFillTop,
      neutralFillTop: neutralFillTop ?? this.neutralFillTop,
      purpleFillTop: purpleFillTop ?? this.purpleFillTop,
      orangeFillTop: orangeFillTop ?? this.orangeFillTop,
      greenFillTop: greenFillTop ?? this.greenFillTop,
      blueFillTop: blueFillTop ?? this.blueFillTop,
      redFillTop: redFillTop ?? this.redFillTop,
      grayFillTop: grayFillTop ?? this.grayFillTop,
      brandFillTop: brandFillTop ?? this.brandFillTop,
      successText: successText ?? this.successText,
      warningText: warningText ?? this.warningText,
      errorText: errorText ?? this.errorText,
      infoText: infoText ?? this.infoText,
      neutralText: neutralText ?? this.neutralText,
      purpleText: purpleText ?? this.purpleText,
      orangeText: orangeText ?? this.orangeText,
      greenText: greenText ?? this.greenText,
      blueText: blueText ?? this.blueText,
      redText: redText ?? this.redText,
      grayText: grayText ?? this.grayText,
      brandText: brandText ?? this.brandText,
      successIcon: successIcon ?? this.successIcon,
      warningIcon: warningIcon ?? this.warningIcon,
      errorIcon: errorIcon ?? this.errorIcon,
      infoIcon: infoIcon ?? this.infoIcon,
      neutralIcon: neutralIcon ?? this.neutralIcon,
      purpleIcon: purpleIcon ?? this.purpleIcon,
      orangeIcon: orangeIcon ?? this.orangeIcon,
      greenIcon: greenIcon ?? this.greenIcon,
      blueIcon: blueIcon ?? this.blueIcon,
      redIcon: redIcon ?? this.redIcon,
      grayIcon: grayIcon ?? this.grayIcon,
      brandIcon: brandIcon ?? this.brandIcon,
    );
  }
}
