part of '../../index.dart';

class WorkspaceCardSelectedColors
    extends ThemeExtension<WorkspaceCardSelectedColors> {
  WorkspaceCardSelectedColors({
    required this.defaultFill,
    required this.hoverFill,
    required this.focusedFill,
    required this.focusedBorder2,
    required this.defaultBorder,
    required this.hoverBorder,
    required this.focusedBorder,
  });

  final Color defaultFill;
  final Color hoverFill;
  final Color focusedFill;
  final Color focusedBorder2;
  final Color defaultBorder;
  final Color hoverBorder;
  final Color focusedBorder;

  @override
  WorkspaceCardSelectedColors lerp(
      WorkspaceCardSelectedColors? other, double t) {
    if (other is! WorkspaceCardSelectedColors) {
      return this;
    }

    return WorkspaceCardSelectedColors(
      defaultFill: Color.lerp(defaultFill, other.defaultFill, t)!,
      hoverFill: Color.lerp(hoverFill, other.hoverFill, t)!,
      focusedFill: Color.lerp(focusedFill, other.focusedFill, t)!,
      focusedBorder2: Color.lerp(focusedBorder2, other.focusedBorder2, t)!,
      defaultBorder: Color.lerp(defaultBorder, other.defaultBorder, t)!,
      hoverBorder: Color.lerp(hoverBorder, other.hoverBorder, t)!,
      focusedBorder: Color.lerp(focusedBorder, other.focusedBorder, t)!,
    );
  }

  @override
  WorkspaceCardSelectedColors copyWith({
    Color? defaultFill,
    Color? hoverFill,
    Color? focusedFill,
    Color? focusedBorder2,
    Color? defaultBorder,
    Color? hoverBorder,
    Color? focusedBorder,
  }) {
    return WorkspaceCardSelectedColors(
      defaultFill: defaultFill ?? this.defaultFill,
      hoverFill: hoverFill ?? this.hoverFill,
      focusedFill: focusedFill ?? this.focusedFill,
      focusedBorder2: focusedBorder2 ?? this.focusedBorder2,
      defaultBorder: defaultBorder ?? this.defaultBorder,
      hoverBorder: hoverBorder ?? this.hoverBorder,
      focusedBorder: focusedBorder ?? this.focusedBorder,
    );
  }
}
