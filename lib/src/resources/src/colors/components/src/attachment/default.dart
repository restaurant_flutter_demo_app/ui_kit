part of '../../index.dart';

class AttachmentDefaultColors extends ThemeExtension<AttachmentDefaultColors> {
  AttachmentDefaultColors({
    required this.text,
  });

  final Color text;

  @override
  AttachmentDefaultColors lerp(AttachmentDefaultColors? other, double t) {
    if (other is! AttachmentDefaultColors) {
      return this;
    }

    return AttachmentDefaultColors(
      text: Color.lerp(text, other.text, t)!,
    );
  }

  @override
  AttachmentDefaultColors copyWith({
    Color? text,
  }) {
    return AttachmentDefaultColors(
      text: text ?? this.text,
    );
  }
}
