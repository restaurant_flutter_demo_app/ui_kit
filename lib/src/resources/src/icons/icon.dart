part of '../../index.dart';

enum IconType {
  remove,
  eyeConturOpen,
  eyeConturClose,
  add,
  close,
  search,
  arrowBack,
  save,
  arrowForward,
}

class IconWidget extends StatelessWidget {
  const IconWidget.size3XS({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.size3XS;

  const IconWidget.size2XS({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.size2XS;

  const IconWidget.sizeXS({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.sizeXS;

  const IconWidget.sizeS({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.sizeS;

  const IconWidget.size2XM({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.size2XM;

  const IconWidget.sizeM({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.sizeM;

  const IconWidget.sizeXL({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.sizeXL;

  const IconWidget.size2XL({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.size2XL;

  const IconWidget.size5XL({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.size5XL;

  final IconType type;
  final double size;
  final Color color;

  @override
  Widget build(BuildContext context) {
    final IconData icon;

    switch (type) {
      case IconType.search:
        icon = Icons.search;
        break;
      case IconType.eyeConturOpen:
        icon = Icons.visibility_outlined;
        break;
      case IconType.eyeConturClose:
        icon = Icons.visibility_off_outlined;
        break;
      case IconType.remove:
        icon = Icons.remove;
        break;
      case IconType.add:
        icon = Icons.add;
        break;
      case IconType.close:
        icon = Icons.close;
        break;
      case IconType.arrowBack:
        icon = Icons.arrow_back;
        break;
      case IconType.save:
        icon = Icons.save;
        break;
      case IconType.arrowForward:
        icon = Icons.arrow_forward;
        break;
    }

    return Icon(
      icon,
      color: color,
      size: size,
    );
  }
}
