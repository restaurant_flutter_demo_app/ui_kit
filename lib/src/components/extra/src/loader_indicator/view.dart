import 'package:flutter/material.dart';
import 'package:ui_kit/src/resources/index.dart';
import 'package:ui_kit/ui_kit.dart';

enum LoaderIndicatorSize {
  lg,
  sm,
}

class APLoaderIndicator extends StatefulWidget {
  const APLoaderIndicator({
    super.key,
  }) : indicatorSize = LoaderIndicatorSize.lg;

  const APLoaderIndicator.sm({
    super.key,
  }) : indicatorSize = LoaderIndicatorSize.sm;

  final LoaderIndicatorSize indicatorSize;

  @override
  State<APLoaderIndicator> createState() => _APLoaderIndicatorState();
}

class _APLoaderIndicatorState extends State<APLoaderIndicator> with TickerProviderStateMixin {
  late AnimationController controller;

  @override
  void initState() {
    controller = AnimationController(
      /// [AnimationController]s can be created with `vsync: this` because of
      /// [TickerProviderStateMixin].
      vsync: this,
      duration: const Duration(seconds: 1),
    )..addListener(() {
        setState(() {});
      });
    controller.repeat(reverse: true);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final progressDefaultColors = theme.extension<ProgressDefaultColors>()!;

    final size = widget.indicatorSize == LoaderIndicatorSize.sm ? Sizing.size2XM : Sizing.sizeXL;

    return Center(
      child: Padding(
        padding: EdgeInsets.all(
          widget.indicatorSize == LoaderIndicatorSize.sm ? Spacing.spacing3XS : Spacing.spacing2XS,
        ),
        child: SizedBox(
          width: size,
          height: size,
          child: CircularProgressIndicator(
            strokeWidth: widget.indicatorSize == LoaderIndicatorSize.sm ? 2.0 : Sizing.size4XS,
            value: controller.value,
            color: progressDefaultColors.fill,
            backgroundColor: Colors.transparent,
          ),
        ),
      ),
    );
  }
}
