part of '../index.dart';

class SearchBarWidget extends StatelessWidget {
  SearchBarWidget({
    super.key,
    required SearchBarModel model,
    this.onCancelTap,
    required this.cancelLabel,
    this.onChanged,
    this.onEditingComplete,
  }) : controller = SearchBarController(model: model);

  const SearchBarWidget.controller({
    super.key,
    required this.controller,
    this.onCancelTap,
    required this.cancelLabel,
    this.onChanged,
    this.onEditingComplete,
  });

  final GestureTapCallback? onCancelTap;
  final String cancelLabel;
  final ValueChanged<String>? onChanged;
  final VoidCallback? onEditingComplete;

  final SearchBarController controller;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;

    return ValueListenableBuilder<SearchBarModel>(
      valueListenable: controller,
      builder: (
        BuildContext context,
        SearchBarModel value,
        Widget? child,
      ) {
        return SizedBox(
          child: Row(
            children: [
              Flexible(
                child: TextEditFormField(
                  prefixIcon: IconWidget.size2XM(
                    color: iconSecondaryColors.secondary70,
                    type: IconType.search,
                  ),
                  controller: controller.textEditingController,
                  onChanged: onChanged,
                  onEditingComplete: onEditingComplete,
                ),
              ),
              const SizedBox(
                width: Spacing.spacing2XS,
              ),
              LabelButtonWidget(
                label: cancelLabel,
                onTap: () {
                  FocusScope.of(context).unfocus();

                  if (controller.query.isEmpty) {
                    return;
                  }

                  controller.textEditingController.clear();

                  if (onCancelTap == null) {
                    return;
                  }

                  onCancelTap!();
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
