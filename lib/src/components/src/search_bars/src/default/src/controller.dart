part of '../index.dart';

class SearchBarController extends ValueNotifier<SearchBarModel> {
  SearchBarController({
    required SearchBarModel model,
  }) : super(
          model,
        );

  final TextEditingController textEditingController = TextEditingController();

  void setText(String v) {
    value = value.copyWith(text: v);
  }

  String get query {
    return textEditingController.text;
  }
}
