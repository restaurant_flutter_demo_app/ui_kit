import 'package:flutter/material.dart';
import 'package:ui_kit/src/components/extra/src/widgets/buttons/label.dart';
import 'package:ui_kit/ui_kit.dart';

export 'src/model.dart';

part 'src/controller.dart';
part 'src/view.dart';
