part of '../index.dart';

class ProductAppBar extends AAppBar {
  const ProductAppBar({
    super.onBack,
    this.onBackTap,
    this.onSaveTap,
    required this.title,
    super.key,
  });

  final GestureTapCallback? onBackTap;
  final GestureTapCallback? onSaveTap;
  final String title;

  @override
  State<ProductAppBar> createState() => _APDefaultAppBarState();
}

class _APDefaultAppBarState extends AAPState<ProductAppBar> {
  @override
  Widget buildContent(BuildContext context) {
    final theme = Theme.of(context);

    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;

    return Row(
      children: [
        GestureDetector(
          onTap: widget.onBackTap,
          child: IconWidget.sizeS(
            color: iconSecondaryColors.secondary70,
            type: IconType.arrowBack,
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TitleAppBar(text: widget.title),
            ],
          ),
        ),
        GestureDetector(
          onTap: widget.onSaveTap,
          child: IconWidget.sizeS(
            color: iconSecondaryColors.secondary70,
            type: IconType.save,
          ),
        ),
      ],
    );
  }
}
