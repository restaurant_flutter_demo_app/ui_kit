part of '../index.dart';

class OrderListAppBar extends AAppBar {
  OrderListAppBar({
    super.onBack,
    this.onAddTap,
    title = '',
    super.key,
  }) : controller = APDefaultAppBarController(
          model: APDefaultAppBarModel(
            title: title,
          ),
        );

  final APDefaultAppBarController controller;
  final GestureTapCallback? onAddTap;

  @override
  State<OrderListAppBar> createState() => _APDefaultAppBarState();
}

class _APDefaultAppBarState extends AAPState<OrderListAppBar> {
  @override
  Widget buildContent(BuildContext context) {
    final theme = Theme.of(context);

    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;

    return ValueListenableBuilder<APDefaultAppBarModel>(
      valueListenable: widget.controller,
      builder: (
        BuildContext context,
        APDefaultAppBarModel value,
        Widget? child,
      ) {
        return Row(
          children: [
            const SizedBox(
              width: Spacing.spacingS,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TitleAppBar(text: widget.controller.value.title),
                ],
              ),
            ),
            GestureDetector(
              onTap: widget.onAddTap,
              child: IconWidget.sizeS(
                color: iconSecondaryColors.secondary70,
                type: IconType.add,
              ),
            ),
          ],
        );
      },
    );
  }
}
