part of '../index.dart';

class OrderAppBar extends AAppBar {
  const OrderAppBar({
    super.onBack,
    this.onBackTap,
    this.onAddTap,
    required this.title,
    super.key,
  });

  final GestureTapCallback? onBackTap;
  final GestureTapCallback? onAddTap;
  final String title;

  @override
  State<OrderAppBar> createState() => _APDefaultAppBarState();
}

class _APDefaultAppBarState extends AAPState<OrderAppBar> {
  @override
  Widget buildContent(BuildContext context) {
    final theme = Theme.of(context);

    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;

    return Row(
      children: [
        GestureDetector(
          onTap: widget.onBackTap,
          child: IconWidget.sizeS(
            color: iconSecondaryColors.secondary70,
            type: IconType.arrowBack,
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TitleAppBar(text: widget.title),
            ],
          ),
        ),
        GestureDetector(
          onTap: widget.onAddTap,
          child: IconWidget.sizeS(
            color: iconSecondaryColors.secondary70,
            type: IconType.add,
          ),
        ),
      ],
    );
  }
}
