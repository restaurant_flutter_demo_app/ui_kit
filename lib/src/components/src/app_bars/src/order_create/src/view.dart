part of '../index.dart';

class OrderCreateAppBar extends AAppBar {
  const OrderCreateAppBar({
    super.onBack,
    this.onBackTap,
    required this.title,
    super.key,
  });

  final GestureTapCallback? onBackTap;
  final String title;

  @override
  State<OrderCreateAppBar> createState() => _APDefaultAppBarState();
}

class _APDefaultAppBarState extends AAPState<OrderCreateAppBar> {
  @override
  Widget buildContent(BuildContext context) {
    final theme = Theme.of(context);

    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;

    return Row(
      children: [
        GestureDetector(
          onTap: widget.onBackTap,
          child: IconWidget.sizeS(
            color: iconSecondaryColors.secondary70,
            type: IconType.arrowBack,
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TitleAppBar(text: widget.title),
            ],
          ),
        ),
        const SizedBox(
          width: Sizing.sizeS,
        ),
      ],
    );
  }
}
