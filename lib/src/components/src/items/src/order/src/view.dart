part of '../index.dart';

class OrderItemWidget extends StatelessWidget {
  OrderItemWidget({
    super.key,
    required OrderItemModel model,
    this.onCloseTap,
  }) : controller = OrderItemController(model: model);

  const OrderItemWidget.controller({
    super.key,
    required this.controller,
    this.onCloseTap,
  });

  final OrderItemController controller;
  final GestureTapCallback? onCloseTap;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;
    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;
    final backgroundColors = theme.extension<BackgroundColors>()!;
    final dividerSecondaryColors = theme.extension<DividerSecondaryColors>()!;

    return ValueListenableBuilder<OrderItemModel>(
      valueListenable: controller,
      builder: (
        BuildContext context,
        OrderItemModel value,
        Widget? child,
      ) {
        return DecoratedBox(
          decoration: BoxDecoration(
            color: backgroundColors.gray,
            borderRadius: const BorderRadius.all(
              Radius.circular(
                Radiuses.radius2XS,
              ),
            ),
            border: Border.all(
              color: dividerSecondaryColors.secondary30,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(
              Spacing.spacing2XS,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  value.name,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: textTheme.headlineLarge!.copyWith(
                    color: textSecondaryColors.secondary100,
                  ),
                ),
                const Spacer(),
                if (value.canRemove) ...[
                  const SizedBox(
                    width: Spacing.spacing2XS,
                  ),
                  GestureDetector(
                    onTap: onCloseTap,
                    child: IconWidget.sizeS(
                      color: iconSecondaryColors.secondary70,
                      type: IconType.close,
                    ),
                  )
                ],
              ],
            ),
          ),
        );
      },
    );
  }
}
