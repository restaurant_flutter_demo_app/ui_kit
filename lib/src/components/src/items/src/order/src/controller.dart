part of '../index.dart';

class OrderItemController extends ValueNotifier<OrderItemModel> {
  OrderItemController({
    required OrderItemModel model,
  }) : super(
          model,
        );

  set likes(String v) {
    value = value.copyWith(name: v);
  }
}
