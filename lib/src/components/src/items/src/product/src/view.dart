part of '../index.dart';

enum ProductItemWidgetState {
  selected,
  unselected,
  disable,
}

class ProductItemWidget extends StatelessWidget {
  const ProductItemWidget({
    super.key,
    required this.priceLabel,
    required this.price,
    required this.name,
    this.state = ProductItemWidgetState.unselected,
  });

  final ProductItemWidgetState state;
  final String priceLabel;
  final String price;
  final String name;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;
    final accentColors = theme.extension<AccentColors>()!;
    final backgroundColors = theme.extension<BackgroundColors>()!;
    final buttonGhostColors = theme.extension<ButtonGhostColors>()!;

    final dividerSecondaryColors = theme.extension<DividerSecondaryColors>()!;

    final Color textColor;

    final Color backgroundColor;

    switch (state) {
      case ProductItemWidgetState.disable:
        backgroundColor = buttonGhostColors.pressedFill;
        textColor = textSecondaryColors.secondary100;
        break;
      case ProductItemWidgetState.selected:
        backgroundColor = accentColors.green;
        textColor = textSecondaryColors.secondary80;
        break;
      case ProductItemWidgetState.unselected:
        backgroundColor = backgroundColors.gray;
        textColor = textSecondaryColors.secondary100;
        break;
    }

    return DecoratedBox(
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: const BorderRadius.all(
          Radius.circular(
            Radiuses.radius2XS,
          ),
        ),
        border: Border.all(
          color: dividerSecondaryColors.secondary30,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(
          Spacing.spacing2XS,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          //mainAxisSize: MainAxisSize.min,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  name,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: textTheme.headlineLarge!.copyWith(
                    color: textColor,
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        '$priceLabel:',
                        style: textTheme.headlineLarge!.copyWith(
                          color: textColor,
                        ),
                      ),
                      const SizedBox(
                        width: Spacing.spacing2XS,
                      ),
                      Text(
                        price,
                        style: textTheme.headlineLarge!.copyWith(
                          color: textColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const Spacer(),
          ],
        ),
      ),
    );
  }
}
