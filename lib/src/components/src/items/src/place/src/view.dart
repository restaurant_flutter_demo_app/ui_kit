part of '../index.dart';

class PlaceItemWidget extends StatelessWidget {
  const PlaceItemWidget({
    super.key,
    required this.name,
  });

  final String name;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;
    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;
    final backgroundColors = theme.extension<BackgroundColors>()!;
    final buttonGhostColors = theme.extension<ButtonGhostColors>()!;

    final dividerSecondaryColors = theme.extension<DividerSecondaryColors>()!;

    final Color textColor = textSecondaryColors.secondary100;

    final Color backgroundColor = backgroundColors.gray;

    return DecoratedBox(
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: const BorderRadius.all(
          Radius.circular(
            Radiuses.radius2XS,
          ),
        ),
        border: Border.all(
          color: dividerSecondaryColors.secondary30,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(
          Spacing.spacing2XS,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          //mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              name,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: textTheme.headlineLarge!.copyWith(
                color: textColor,
              ),
            ),
            const Spacer(),
            const SizedBox(
              width: Spacing.spacing2XS,
            ),
            IconWidget.sizeS(
              color: iconSecondaryColors.secondary70,
              type: IconType.arrowForward,
            )
          ],
        ),
      ),
    );
  }
}
