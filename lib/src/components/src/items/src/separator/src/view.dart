part of '../index.dart';

class SeparatorItemWidget extends StatelessWidget {
  const SeparatorItemWidget({
    super.key,
    required this.name,
  });

  final String name;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;

    final Color textColor = textSecondaryColors.secondary100;

    return Padding(
      padding: const EdgeInsets.all(
        Spacing.spacing2XS,
      ),
      child: Text(
        name,
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
        style: textTheme.headlineLarge!.copyWith(
          color: textColor,
        ),
      ),
    );
  }
}
