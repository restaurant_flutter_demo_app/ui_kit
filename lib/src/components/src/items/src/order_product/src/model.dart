import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class OrderProductItemModel with _$OrderProductItemModel {
  const factory OrderProductItemModel({
    required String name,
    required int count,
    required bool canUpdate,
  }) = _OrderProductItemModel;
}
