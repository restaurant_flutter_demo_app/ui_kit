part of '../index.dart';

class OrderProductItemWidget extends StatelessWidget {
  OrderProductItemWidget({
    super.key,
    required OrderProductItemModel model,
    this.onCloseTap,
    this.onRemoveTap,
    this.onAddTap,
  }) : controller = OrderProductItemController(model: model);

  const OrderProductItemWidget.controller({
    super.key,
    required this.controller,
    this.onCloseTap,
    this.onRemoveTap,
    this.onAddTap,
  });

  final OrderProductItemController controller;

  final GestureTapCallback? onCloseTap;
  final GestureTapCallback? onRemoveTap;
  final GestureTapCallback? onAddTap;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;
    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;
    final backgroundColors = theme.extension<BackgroundColors>()!;
    final dividerSecondaryColors = theme.extension<DividerSecondaryColors>()!;

    return ValueListenableBuilder<OrderProductItemModel>(
      valueListenable: controller,
      builder: (
        BuildContext context,
        OrderProductItemModel value,
        Widget? child,
      ) {
        return DecoratedBox(
          decoration: BoxDecoration(
            color: backgroundColors.gray,
            borderRadius: const BorderRadius.all(
              Radius.circular(
                Radiuses.radius2XS,
              ),
            ),
            border: Border.all(
              color: dividerSecondaryColors.secondary30,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(
              Spacing.spacing2XS,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              //mainAxisSize: MainAxisSize.min,
              children: [
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      value.name,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: textTheme.headlineLarge!.copyWith(
                        color: textSecondaryColors.secondary100,
                      ),
                    ),
                    const SizedBox(
                      height: Spacing.spacing2XS,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        if (value.canUpdate)
                          GestureDetector(
                            onTap: onRemoveTap,
                            child: IconWidget.size2XM(
                              color: iconSecondaryColors.secondary70,
                              type: IconType.remove,
                            ),
                          ),
                        const SizedBox(
                          width: Spacing.spacing2XS,
                        ),
                        Text(
                          '${value.count}',
                          style: textTheme.headlineLarge!.copyWith(
                            color: textSecondaryColors.secondary100,
                          ),
                        ),
                        const SizedBox(
                          width: Spacing.spacing2XS,
                        ),
                        if (value.canUpdate)
                          GestureDetector(
                            onTap: onAddTap,
                            child: IconWidget.size2XM(
                              color: iconSecondaryColors.secondary70,
                              type: IconType.add,
                            ),
                          )
                      ],
                    ),
                  ],
                ),
                const Spacer(),
                if (value.canUpdate) ...[
                  const SizedBox(
                    width: Spacing.spacing2XS,
                  ),
                  GestureDetector(
                    onTap: onCloseTap,
                    child: IconWidget.sizeS(
                      color: iconSecondaryColors.secondary70,
                      type: IconType.close,
                    ),
                  )
                ],
              ],
            ),
          ),
        );
      },
    );
  }
}
