part of '../index.dart';

class OrderProductItemController extends ValueNotifier<OrderProductItemModel> {
  OrderProductItemController({
    required OrderProductItemModel model,
  }) : super(
          model,
        );

  void updateCount(int v) {
    value = value.copyWith(count: v);
  }
}
