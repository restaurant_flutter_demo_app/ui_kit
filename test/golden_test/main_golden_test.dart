import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:golden_toolkit/golden_toolkit.dart';
import 'package:intl/intl.dart';
import 'package:ui_kit/ui_kit.dart';

const _surfaceWidth = 800.0;

void main() async {
  await loadAppFonts();

  const defaultTheme = 'defaultTheme';

  group(
    'app_bar/',
    () {
      const path = '$defaultTheme/app_bar/';

      testGoldens('app_bar', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'DefaultAppBar()',
            _GoldenTestWrapperWidget(
              child: OrderListAppBar(title: 'Restaurant Orders'),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.defaultTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}DefaultAppBar',
          autoHeight: true,
        );
      });
    },
  );

  group(
    'item/',
    () {
      const path = '$defaultTheme/item/';

      testGoldens('item', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'OrderItemWidget(canRemove: true)',
            _GoldenTestWrapperWidget(
              child: OrderItemWidget(
                model: const OrderItemModel(
                  name: "Foo bar",
                  canRemove: true,
                ),
              ),
            ),
          )
          ..addScenario(
            'OrderItemWidget(canRemove: false)',
            _GoldenTestWrapperWidget(
              child: OrderItemWidget(
                model: const OrderItemModel(
                  name: "Foo bar",
                  canRemove: false,
                ),
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.defaultTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}OrderItemWidget',
          autoHeight: true,
        );
      });
    },
  );

  group(
    'item/',
    () {
      const path = '$defaultTheme/item/';

      testGoldens('item', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'OrderProductItemWidget(canUpdate: true)',
            _GoldenTestWrapperWidget(
              child: OrderProductItemWidget(
                model: const OrderProductItemModel(
                  name: "Foo bar Foo bar Foo bar Foo bar",
                  canUpdate: true,
                  count: 10,
                ),
              ),
            ),
          )
          ..addScenario(
            'OrderProductItemWidget(canUpdate: false)',
            _GoldenTestWrapperWidget(
              child: OrderProductItemWidget(
                model: const OrderProductItemModel(
                  name: "Foo bar",
                  canUpdate: false,
                  count: 10,
                ),
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.defaultTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}OrderProductItemWidget',
          autoHeight: true,
        );
      });
    },
  );

  group(
    'item/',
    () {
      const path = '$defaultTheme/item/';

      var formatter = NumberFormat('00');

      testGoldens('item', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'ProductItemWidget(state: .selected)',
            _GoldenTestWrapperWidget(
              child: ProductItemWidget(
                name: "FooBar",
                state: ProductItemWidgetState.selected,
                priceLabel: "Price",
                price: '${12}.${formatter.format(5)}',
              ),
            ),
          )
          ..addScenario(
            'ProductItemWidget(state: .unselected)',
            const _GoldenTestWrapperWidget(
              child: ProductItemWidget(
                name: "FooBar",
                state: ProductItemWidgetState.unselected,
                priceLabel: "Price",
                price: "12.00",
              ),
            ),
          )
          ..addScenario(
            'ProductItemWidget(state: .disable)',
            const _GoldenTestWrapperWidget(
              child: ProductItemWidget(
                name: "FooBar",
                state: ProductItemWidgetState.disable,
                priceLabel: "Price",
                price: "12.00",
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.defaultTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}ProductItemWidget',
          autoHeight: true,
        );
      });
    },
  );

  group(
    'search_bars/default/',
    () {
      const path = '$defaultTheme/search_bars/default/';

      testGoldens('item', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'SearchBarWidget()',
            _GoldenTestWrapperWidget(
              child: SearchBarWidget(
                cancelLabel: 'Cancel',
                model: const SearchBarModel(text: ''),
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.defaultTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}SearchBarWidget',
          autoHeight: true,
        );
      });
    },
  );
}

class _GoldenTestWrapperWidget extends StatelessWidget {
  const _GoldenTestWrapperWidget({
    required this.child,
  });

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Center(
        child: child,
      ),
    );
  }
}
